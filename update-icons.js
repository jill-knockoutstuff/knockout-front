#!/usr/bin/env node
// @ts-check
const fs = require('fs');

const marker = `  /* SPLICE HERE */`;
const prefix = 'static/icons';

console.log(`Working from ${process.cwd()}`);

const icons = require('./config/icons');
const newIcons = [];

// Build a mapping from filename to icon object for fast lookup
const iconMap = icons.reduce((map, icon) => Object.assign(map, { [icon.url]: icon }), {});

// Get the highest ID already assigned in the icon list
let highestId = icons.reduce((max, icon) => icon.id > max ? icon.id : max, 0);

// Get the icons in the folder
const fileList = fs.readdirSync(prefix);
fileList.forEach(file => {
    const path = `${prefix}/${file}`;
    if (path in iconMap) {
        console.log(`Found (old): ${path}`);
    }
    else {
        console.log(`Found (new): ${path}`);
        newIcons.push({ path, id: ++highestId });
    }
});

// Put the new icons in the file
if (newIcons.length > 0) {
    const iconFile = fs.readFileSync('./config/icons.js', { flag: 'r+', encoding: 'utf-8' });
    const spliceIndex = iconFile.indexOf(marker);
    if (spliceIndex < 0) {
        throw new Error(`Special marker not found: ${marker}, aborting`);
    }
    else {
        let newIconFile = iconFile.slice(0, spliceIndex);
        newIconFile += JSON.stringify(newIcons, null, 2).replace(/"(.+)":/g, "$1:").replace(/"/g, `'`).replace(/\[\n|\]/g, ``);
        newIconFile += iconFile.slice(spliceIndex);
        fs.writeFileSync('./config/icons.js', newIconFile);
    }
}
