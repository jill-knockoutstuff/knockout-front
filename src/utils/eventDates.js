import dayjs from 'dayjs';

const shouldDoUkraineOverride = () => {
  // Special override to enable Ukraine themed logo and "FREE UKRAINE"/"STAND WITH UKRAINE"/etc. quotes
  return true;
};

export const getEventHeaderLogo = () => {
  // Ukraine override is prioritised over everything else
  if (shouldDoUkraineOverride()) {
    return {
      default: '/static/logo_ukraine.png ',
      light: '/static/logo_ukraine_light.png',
      dark: '/static/logo_ukraine.png',
    };
  }

  const currentDate = dayjs();

  if (currentDate.month() === 9) {
    return { default: '/static/logo_october.png' };
  }

  if (currentDate.month() === 3 && currentDate.date() === 1 && currentDate.year() === 2021) {
    return { default: '/static/logo_april_2021.png' };
  }

  const christmasDate = dayjs(`12/25/${currentDate.year()}`);

  if (Math.abs(currentDate.diff(christmasDate, 'day')) <= 7) {
    return { default: '/static/logo_summer.svg' };
  }

  return null;
};

export const getEventColor = () => {
  const currentDate = dayjs();

  if (currentDate.month() === 9) {
    return '#ec7337';
  }

  const christmasDate = dayjs(`12/25/${currentDate.year()}`);

  if (Math.abs(currentDate.diff(christmasDate, 'day')) <= 7) {
    return '#ff3b4e';
  }

  return null;
};

export const getEventText = () => {
  const currentDate = dayjs();

  if (currentDate.month() === 9) {
    return 'spookout';
  }

  if (currentDate.month() === 3 && currentDate.date() === 1 && currentDate.year() === 2021) {
    return 'amogus';
  }

  return 'knockout';
};

export const getEventQuotes = () => {
  // Ukraine override is prioritised over everything else
  if (shouldDoUkraineOverride()) {
    return [
      // Main Ukraine override quotes, based on the main hashtags that have been used around
      'Free Ukraine',
      'Stand with Ukraine',

      // These ones are just extra ones for more flavour, can disable whichever ones we don't want
      'Stop Putin',
      '"Russian warship, go fuck yourself"',
      'No to war',
    ];
  }

  const currentDate = dayjs();

  const christmasDate = dayjs(`12/25/${currentDate.year()}`);

  if (Math.abs(currentDate.diff(christmasDate, 'day')) <= 7) {
    return [
      'Happy holidays!',
      "Summer's here!",
      "It's getting hot in here 🥵",
      'Ever wonder how Santa manages to wear those heavy clothes in December?',
      '☀️',
      '2020 edition',
      '🏄',
      '🎄',
      'This year, my gift to you is anime memes 🎁',
      'Brazilian Christmas Edition',
      "We didn't have enough money to put snow on the site this year, so we're going with a summer theme",
      'Cultural exchange edition',
      '"During summer temperatures can often reach 40 degrees Celsius (86 to 104 degrees Fahrenheit) in Rio de Janeiro and..."',
      "There... there's actually people in the other hemisphere?",
    ];
  }

  return null;
};
