export const DELETED_USER_USERNAME = 'DELETED_USER';
export const isDeletedUser = (username) => username === DELETED_USER_USERNAME;
