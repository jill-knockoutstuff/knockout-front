export const SUBSCRIPTION_SET_STATE = 'SUBSCRIPTION_SET_STATE';
export const SUBSCRIPTION_UPDATE_THREAD = 'SUBSCRIPTION_UPDATE_THREAD';
export const SUBSCRIPTION_REMOVE_THREAD = 'SUBSCRIPTION_REMOVE_THREAD';
export const SUBSCRIPTION_ADD_THREAD = 'SUBSCRIPTION_ADD_THREAD';

export function setSubscriptions(value) {
  return {
    type: SUBSCRIPTION_SET_STATE,
    value,
  };
}

export function addSubscriptionThread(value) {
  return {
    type: SUBSCRIPTION_ADD_THREAD,
    value,
  };
}

export function updateSubscriptionThread(value) {
  return {
    type: SUBSCRIPTION_UPDATE_THREAD,
    value,
  };
}

export function removeSubscriptionThread(value) {
  return {
    type: SUBSCRIPTION_REMOVE_THREAD,
    value,
  };
}
