import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import socketClient from '../../../socketClient';
import {
  ThemeFontFamily,
  ThemeFontSizeLarge,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';

const ViewerCount = ({ id, viewers, onClick }) => {
  const [members, setMembers] = useState(viewers.memberCount);
  const [guests, setGuests] = useState(viewers.guestCount);

  useEffect(() => {
    setMembers(viewers.memberCount);
    setGuests(viewers.guestCount);
  }, [viewers]);

  useEffect(() => {
    socketClient.emit('thread:join', id);
    return () => socketClient.emit('thread:leave', id);
  }, [id]);

  useEffect(() => {
    socketClient.on('thread:members', (value) => {
      setMembers(value);
    });
    socketClient.on('thread:guests', (value) => {
      setGuests(value);
    });

    return () => {
      socketClient.off('thread:members');
      socketClient.off('thread:guests');
    };
  }, [id]);

  const memberText = members + (members === 1 ? ' member ' : ' members ');
  const guestText = guests + (guests === 1 ? ' guest ' : ' guests ');

  return (
    <StyledViewerCount onClick={onClick} disabled={!viewers.users}>
      <i className="fas fa-user-friends viewer-icon" />
      <span className="viewer-text">
        {`${members ? memberText : ''}${members && guests ? 'and ' : ''}${guests ? guestText : ''}${
          members || guests ? 'reading now' : 'No viewers'
        }`}
      </span>
    </StyledViewerCount>
  );
};

ViewerCount.propTypes = {
  id: PropTypes.number.isRequired,
  viewers: PropTypes.shape({
    memberCount: PropTypes.number.isRequired,
    guestCount: PropTypes.number.isRequired,
    users: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  onClick: PropTypes.func.isRequired,
};

const StyledViewerCount = styled.button`
  display: flex;
  align-items: center;
  margin-left: ${ThemeHorizontalPadding};
  margin-top: calc(${ThemeVerticalPadding} / 2);
  margin-bottom: ${ThemeVerticalPadding};
  border: none;
  background: transparent;
  color: ${ThemeTextColor};
  font-family: ${ThemeFontFamily};
  padding: 0;
  transition: 0.3s;
  cursor: pointer;

  &:active {
    padding: 0;
  }

  &:hover .viewer-text,
  &:hover .viewer-icon {
    opacity: 0.9;
  }

  &:disabled {
    pointer-events: none;
  }

  .viewer-icon {
    font-size: 22px;
    margin-right: 5px;
    opacity: ${(props) => (props.theme.mode === 'light' ? 0.8 : 0.6)};
    transition: 0.3s;
  }

  .viewer-text {
    font-size: ${ThemeFontSizeLarge};
    opacity: ${(props) => (props.theme.mode === 'light' ? 0.8 : 0.6)};
    font-weight: 600;
    transition: 0.3s;
  }
`;
export default ViewerCount;
