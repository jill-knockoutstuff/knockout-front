import axios from 'axios';
import config from '../../config';
import { authPut } from './common';

export const getTags = async () => {
  try {
    const results = await axios.get(`${config.apiHost}/tag/list`);

    return results.data;
  } catch (err) {
    console.error(err);
    return [];
  }
};

export const updateThreadTags = async (id, data) => {
  try {
    await authPut({ url: `/v2/threads/${id}/tags`, data });
  } catch (err) {
    console.error(err);
    throw err;
  }
};
