import { authDelete, authPut } from './common';

export const submitRating = ({ postId, rating }) => {
  const requestBody = { rating };
  return authPut({ url: `/v2/posts/${postId}/ratings`, data: requestBody });
};

export const removeRating = (postId) => {
  return authDelete({ url: `/v2/posts/${postId}/ratings` });
};
