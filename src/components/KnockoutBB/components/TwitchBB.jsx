import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

/**
 * A styled image block component.
 *
 * @type {Component}
 */
const StyledTwitchWrapper = styled.div`
  display: block;
  margin: 15px 0;

  position: relative;
  width: 100%;
  max-width: 960px;

  &:before {
    content: '';
    width: 1px;
    margin-left: -1px;
    float: left;
    height: 0;
    padding-top: calc(540 / 960 * 100%);
  }
  &:after {
    content: '';
    display: table;
    clear: both;
  }

  .twitch-inner {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  ${(props) => props.selected && `box-shadow: 0px 0px 0 1px #2900ff;`}
`;

export const getTwitchId = (src) => {
  // Three scenarios:
  // Channel / live stream: twitch.tv/{channel name}
  // VOD: twitch.tv/videos/{VOD id}
  // Clip: twitch.tv/{channel name}/clip/{Clip id}
  try {
    const url = new URL(src);
    if (!url.hostname.includes('twitch.tv')) throw new Error();
    const path = url.pathname.split('/');
    if (url.hostname === 'clips.twitch.tv') {
      return {
        type: 'clip',
        id: url.pathname.slice(1),
      };
    }
    if (path.length === 2) {
      return {
        type: 'channel',
        id: path[1],
      };
    }
    if (path.length === 3 && path[1] === 'videos') {
      return {
        type: 'video',
        id: path[2],
      };
    }
    if (path.length === 4 && path[2] === 'clip') {
      return {
        type: 'clip',
        id: path[3],
      };
    }
    return null;
  } catch (error) {
    return null;
  }
};

const TwitchBB = ({ href, children }) => {
  try {
    const url = href || children.join('');
    const twitchEmbed = getTwitchId(url);

    if (twitchEmbed === null) {
      throw new Error('Bad Twitch embed');
    }

    const { type, id } = twitchEmbed;

    if (type === 'channel' || type === 'video') {
      return (
        <StyledTwitchWrapper>
          <iframe
            className="twitch-inner"
            title={`twitch-${id}`}
            src={`https://player.twitch.tv/?${type}=${id}&parent=${window.location.hostname}&autoplay=false`}
            height="720"
            width="1280"
            frameBorder="0"
            scrolling="yes"
            allowFullScreen="true"
          />
        </StyledTwitchWrapper>
      );
    }
    if (type === 'clip') {
      return (
        <StyledTwitchWrapper>
          <iframe
            className="twitch-inner"
            title={`twitch-${id}`}
            src={`https://clips.twitch.tv/embed?clip=${id}&parent=${window.location.hostname}&autoplay=false`}
            height="720"
            width="1280"
            frameBorder="0"
            scrolling="yes"
            allowFullScreen="true"
          />
        </StyledTwitchWrapper>
      );
    }
    return '[Bad Twitch embed.]';
  } catch (error) {
    console.log(error);
    return '[Bad Twitch embed.]';
  }
};

TwitchBB.propTypes = {
  children: PropTypes.arrayOf(PropTypes.string),
  href: PropTypes.string,
};

TwitchBB.defaultProps = {
  children: [],
  href: '',
};

export default TwitchBB;
