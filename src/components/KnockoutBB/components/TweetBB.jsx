/* eslint-disable no-underscore-dangle */
import React, { useContext, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';

export const getTweetId = (src) => {
  const twitterRegx = /^https?:\/\/(?:mobile\.)?twitter\.com\/(?:#!\/)?(\w+)\/status(es)?\/(\d+)/;

  const tweet = twitterRegx.exec(src);
  if (!tweet) return null;
  const tweetId = tweet[3];
  if (!tweetId) return null;

  return tweetId;
};

const TweetBB = ({ href, children }) => {
  const theme = useContext(ThemeContext);
  const tweetUrl = href || children.join('');
  const tweetId = getTweetId(tweetUrl);
  const tweetRef = useRef();

  useEffect(() => {
    window.twttr = ((d, s, id) => {
      const fjs = d.getElementsByTagName(s)[0];
      const t = window.twttr || {};
      if (d.getElementById(id)) return t;
      const js = d.createElement(s);
      js.id = id;
      js.src = 'https://platform.twitter.com/widgets.js';
      fjs.parentNode.insertBefore(js, fjs);

      t._e = [];
      t.ready = (f) => {
        t._e.push(f);
      };

      return t;
    })(document, 'script', 'twitter-wjs');

    window.twttr.ready((twttr) => {
      twttr.widgets.createTweet(tweetId, tweetRef.current, {
        theme: theme.mode === 'light' ? 'light' : 'dark',
      });
    });
  }, [theme.mode, tweetId]);

  return <div ref={tweetRef} />;
};

TweetBB.propTypes = {
  href: PropTypes.string.isRequired,
  children: PropTypes.arrayOf(PropTypes.node).isRequired,
};

export default TweetBB;
