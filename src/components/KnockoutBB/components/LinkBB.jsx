/* eslint-disable react/jsx-props-no-spreading */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Tooltip from '../../Tooltip';
import getOpenGraphData from '../../../services/openGraph';
import SmartLink from './SmartLinkBB';

const StyledToggleContainer = styled.div`
  display: inline-block;
`;

export const StyledAnchor = styled.a`
  color: #3facff;
  text-decoration: underline;
  cursor: pointer;

  &:hover {
    filter: brightness(1.3);
  }
`;

const LinkBB = ({ href, isSmart, children }) => {
  const [openGraphData, setOpenGraphData] = useState(null);

  const getOgData = async () => {
    if (!href) {
      return;
    }
    const ogData = await getOpenGraphData(href);

    if (ogData) {
      setOpenGraphData(ogData);
    }
  };

  useEffect(() => {
    if (isSmart) {
      getOgData();
    }
  }, [isSmart]);

  const childrenString = typeof children === 'string' ? children : 'Link';

  const url = href || childrenString;
  const display = children && children.length > 0 ? childrenString : `${href}`;

  return (
    <>
      {isSmart && openGraphData ? (
        <SmartLink linkUrl={href || childrenString} {...openGraphData} />
      ) : (
        <>
          <StyledAnchor href={url} target="_blank" rel="noopener ugc">
            {display}
          </StyledAnchor>

          {isSmart && (
            <StyledToggleContainer>
              <Tooltip text="Attempting to fetch link information...">
                &nbsp;
                <i className="fas fa-cloud-download-alt" />
              </Tooltip>
            </StyledToggleContainer>
          )}
        </>
      )}
    </>
  );
};

LinkBB.propTypes = {
  editable: PropTypes.bool.isRequired,
  href: PropTypes.string,
  isSmart: PropTypes.bool,
  children: PropTypes.node.isRequired,
};
LinkBB.defaultProps = {
  href: undefined,
  isSmart: undefined,
};

export default LinkBB;
