import React from 'react';

export const getSoundCloudId = (src) => {
  try {
    const url = new URL(src);
    if (!url.hostname.includes('soundcloud.com')) throw new Error();
    const path = url.pathname.split('/');
    return path.slice(-2).join('/');
  } catch (error) {
    return null;
  }
};

const SoundCloudBB = ({ href, children }) => {
  try {
    const url = href || children.join('');
    const soundCloudId = getSoundCloudId(url);

    const src = `https://w.soundcloud.com/player/?url=https%3A//soundcloud.com/${soundCloudId}&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true`;

    return (
      <iframe
        src={src}
        width="800"
        height="300"
        frameBorder="0"
        title={`soundcloud-${soundCloudId}`}
      />
    );
  } catch (error) {
    return '[Bad SoundCloud embed.]';
  }
};

export default SoundCloudBB;
