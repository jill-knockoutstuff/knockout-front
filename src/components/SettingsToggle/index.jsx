import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FieldLabel, FieldLabelSmall } from '../FormControls';
import FormSwitch from '../FormControls/components/FormSwitch';

const SettingsToggle = ({ label, desc, value, setValue }) => (
  <StyledSettingsToggle>
    <div>
      <FieldLabel>{label}</FieldLabel>
      <FieldLabelSmall>{desc}</FieldLabelSmall>
    </div>
    <FormSwitch checked={value} toggle={setValue} />
  </StyledSettingsToggle>
);

SettingsToggle.propTypes = {
  label: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  value: PropTypes.bool.isRequired,
  setValue: PropTypes.func.isRequired,
};

const StyledSettingsToggle = styled.label`
  display: flex;
  justify-content: space-between;
  line-height: normal;
  align-items: center;
  margin-bottom: 20px;
  padding: 0 20px;

  ${FieldLabel} {
    margin-bottom: 5px;
    font-weight: 600;
  }

  ${FieldLabelSmall} {
    margin-bottom: 0px;
  }
`;

export default SettingsToggle;
