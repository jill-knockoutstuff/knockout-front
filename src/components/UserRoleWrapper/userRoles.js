/* stylelint-disable property-no-vendor-prefix */
import { css } from 'styled-components';
import {
  ThemeBannedUserColor,
  ThemeGoldMemberColor,
  ThemeGoldMemberGlow,
  ThemeMemberColor,
  ThemeModeratorColor,
  ThemeModeratorInTrainingColor,
} from '../../utils/ThemeNew';

export default {
  'banned-user': {
    name: 'Banned User',
    color: ThemeBannedUserColor,
    extraStyle: null,
  },
  'limited-user': {
    name: 'Member',
    color: ThemeMemberColor,
  },
  'basic-user': {
    name: 'Member',
    color: ThemeMemberColor,
  },
  'gold-user': {
    name: 'Gold Member',
    color: '#fcbe20',
    extraStyle: css`
      color: transparent;
      filter: ${ThemeGoldMemberGlow};
      background: ${ThemeGoldMemberColor};
      background-clip: text;
      text-shadow: unset;
      -webkit-background-clip: text;
    `,
  },
  moderator: {
    name: 'Moderator',
    color: ThemeModeratorColor,
    extraStyle: null,
  },
  admin: {
    name: 'Admin',
    color: '#fcbe20',
    extraStyle: css`
      color: transparent;
      filter: ${ThemeGoldMemberGlow};
      background: ${ThemeGoldMemberColor};
      background-clip: text;
      -webkit-background-clip: text;
    `,
  },
  'moderator-in-training': {
    name: 'Moderator in Training',
    color: ThemeModeratorInTrainingColor,
    extraStyle: null,
  },
};
