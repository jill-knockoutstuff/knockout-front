import dayjs from 'dayjs';
import React from 'react';
import Message from '../../../../src/views/MessagesPage/components/Message';
import { customRender } from '../../../custom_renderer';

describe('Message component', () => {
  const user = { id: 2, username: 'Rick', avatarUrl: '', usergroup: 1 };

  it('displays message information', async () => {
    const { queryByText, queryByTitle } = customRender(
      <Message
        user={user}
        createdAt={dayjs().subtract(1, 'day').toISOString()}
        readAt={dayjs().toISOString()}
        content="Here's a message"
      />
    );
    expect(queryByText('Rick')).not.toBeNull();
    expect(queryByText('1d')).not.toBeNull();
    expect(queryByTitle("Rick's Avatar")).not.toBeNull();
    expect(queryByText("Here's a message")).not.toBeNull();
    expect(queryByText('Seen just now')).not.toBeNull();
  });

  it('hides read message', async () => {
    const { queryByTestId } = customRender(
      <Message
        user={user}
        createdAt={dayjs().subtract(1, 'day').toISOString()}
        content="Here's a message"
      />
    );
    expect(queryByTestId('read-message')).toBeNull();
  });

  it('hides user information', async () => {
    const { queryByText, queryByTitle } = customRender(
      <Message
        user={user}
        createdAt={dayjs().subtract(1, 'day').toISOString()}
        readAt={dayjs().toISOString()}
        content="Here's a message"
        child
      />
    );
    expect(queryByText('Rick')).toBeNull();
    expect(queryByText('1d')).toBeNull();
    expect(queryByTitle("Rick's Avatar")).toBeNull();
  });
});
