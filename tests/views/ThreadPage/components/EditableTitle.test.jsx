/* eslint-disable no-underscore-dangle */
import React from 'react';

import EditableTitle from '../../../../src/views/ThreadPage/components/EditableTitle';
import { customRender } from '../../../custom_renderer';

describe('EditableTitle component', () => {
  const defaultState = {};

  beforeEach(() => {
    localStorage.clear();
  });

  it('displays the thread title', () => {
    const { queryByText } = customRender(<EditableTitle title="Thread title" />);
    expect(queryByText('Thread title')).not.toBeNull();
  });

  describe('as a non-logged in user', () => {
    it('does not display edit controls', () => {
      const { queryByTitle } = customRender(<EditableTitle title="Thread title" />);
      expect(queryByTitle('Edit title')).toBeNull();
    });
  });

  describe('as a logged in user', () => {
    const loggedInState = {
      ...defaultState,
      user: {
        loggedIn: true,
        username: 'TestUser',
      },
    };

    const userLocalStorageDetails = {
      id: 123,
      username: 'TestUser',
      usergroup: 1,
      avatarUrl: 'avatar.png',
    };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);
    });

    describe('who is not the thread creator', () => {
      it('does not display edit controls', () => {
        const { queryByTitle } = customRender(<EditableTitle title="Thread title" />, {
          initialState: loggedInState,
        });
        expect(queryByTitle('Edit title')).toBeNull();
      });
    });

    describe('who is the thread creator', () => {
      it('displays edit controls', () => {
        const { queryByTitle, queryByText } = customRender(
          <EditableTitle title="Thread title" byCurrentUser />,
          { initialState: loggedInState }
        );
        expect(queryByTitle('Edit title')).not.toBeNull();
        queryByTitle('Edit title').click();
        expect(queryByText('Save')).not.toBeNull();
        expect(queryByText('Cancel')).not.toBeNull();
      });
    });

    describe('who is a moderator', () => {
      const moderatorState = {
        user: {
          loggedIn: true,
          username: 'TestUser',
          usergroup: 3,
        },
      };

      it('displays edit controls', () => {
        const { queryByTitle, queryByText } = customRender(
          <EditableTitle title="Thread title" byCurrentUser />,
          { initialState: moderatorState }
        );
        expect(queryByTitle('Edit title')).not.toBeNull();
        queryByTitle('Edit title').click();
        expect(queryByText('Save')).not.toBeNull();
        expect(queryByText('Cancel')).not.toBeNull();
      });
    });
  });
});
