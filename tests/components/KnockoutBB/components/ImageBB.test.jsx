import '@testing-library/jest-dom/extend-expect';
import { fireEvent } from '@testing-library/react';
import React from 'react';
import ImageBB from '../../../../src/components/KnockoutBB/components/ImageBB';
import PostContext from '../../../../src/components/Post/PostContext';
import { customRender } from '../../../custom_renderer';

describe('Post Image', () => {
  const user = {
    id: 3,
    createdAt: new Date(2021, 1, 1).toISOString(),
    posts: 20,
  };

  it('displays the image', () => {
    const { queryByAltText } = customRender(
      <ImageBB href="link" thumbnail={false} link={false} user={user} />
    );
    expect(queryByAltText('post embed')).not.toBeNull();
  });

  it('displays a linked image', () => {
    const { queryByAltText, queryByRole } = customRender(
      <ImageBB href="link" thumbnail={false} link user={user} />
    );
    expect(queryByAltText('post embed')).not.toBeNull();
    expect(queryByRole('link')).not.toBeNull();
  });

  it('displays a thumbnail', () => {
    const { queryByAltText, queryByRole } = customRender(
      <ImageBB href="link" thumbnail link={false} user={user} />
    );
    expect(queryByAltText('post embed')).not.toBeNull();
    expect(queryByRole('link')).not.toBeNull();
  });

  it('displays a spoilered image', () => {
    const { queryByAltText, queryByRole } = customRender(
      <ImageBB href="link" thumbnai={false} link={false} spoiler user={user} />
    );
    expect(queryByAltText('post embed')).not.toBeNull();
    expect(queryByRole('button')).not.toBeNull();
  });

  it('displays a warning for new accounts', () => {
    const newUser = {
      id: 3,
      createdAt: new Date().toISOString(),
      posts: 20,
    };
    const { queryByAltText, queryByRole, queryByText } = customRender(
      <PostContext.Provider value={{ user: newUser, postEdited: new Date().toISOString() }}>
        <ImageBB href="link" thumbnai={false} link={false} />
      </PostContext.Provider>
    );
    expect(queryByAltText('post embed')).not.toBeNull();

    const button = queryByRole('button');
    expect(button).not.toBeNull();

    fireEvent.click(button);
    expect(queryByText('View image?')).not.toBeNull();
  });

  it('displays a warning for accounts with low post counts', () => {
    const newUser = {
      id: 3,
      createdAt: new Date(2021, 1, 1).toISOString(),
      posts: 5,
    };
    const { queryByAltText, queryByRole, queryByText } = customRender(
      <PostContext.Provider value={{ user: newUser, postEdited: new Date().toISOString() }}>
        <ImageBB href="link" thumbnai={false} link={false} />
      </PostContext.Provider>
    );
    expect(queryByAltText('post embed')).not.toBeNull();

    const button = queryByRole('button');
    expect(button).not.toBeNull();

    fireEvent.click(button);
    expect(queryByText('View image?')).not.toBeNull();
  });

  it('reveals images for new accounts in posts older than 12 hours', () => {
    const newUser = {
      id: 3,
      createdAt: new Date().toISOString(),
      posts: 20,
    };
    const { queryByAltText, queryByRole } = customRender(
      <PostContext.Provider
        value={{ user: newUser, postEdited: new Date(2021, 1, 1).toISOString() }}
      >
        <ImageBB href="link" thumbnai={false} link={false} />
      </PostContext.Provider>
    );
    expect(queryByAltText('post embed')).not.toBeNull();

    const button = queryByRole('button');
    expect(button).toBeNull();
  });

  it('reveals images for accounts with low posts counts in posts older than 12 hours', () => {
    const newUser = {
      id: 3,
      createdAt: new Date(2021, 1, 1).toISOString(),
      posts: 5,
    };
    const { queryByAltText, queryByRole } = customRender(
      <PostContext.Provider
        value={{ user: newUser, postEdited: new Date(2021, 1, 1).toISOString() }}
      >
        <ImageBB href="link" thumbnai={false} link={false} />
      </PostContext.Provider>
    );
    expect(queryByAltText('post embed')).not.toBeNull();

    const button = queryByRole('button');
    expect(button).toBeNull();
  });
});
