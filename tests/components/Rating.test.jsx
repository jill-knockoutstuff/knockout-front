/* eslint-disable no-underscore-dangle */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { act } from '@testing-library/react';
import { customRender } from '../custom_renderer';
import RatingBar from '../../src/components/Rating/RatingBar';

describe('RatingBar component', () => {
  let defaultProps = {};
  const initialState = { user: { loggedIn: true } };

  beforeEach(() => {
    defaultProps = {
      postId: 0,
      xrayEnabled: false,
      ratings: [],
      ratingDisabled: false,
      refreshPost: jest.fn(),
    };
  });

  it('hides the rating x-ray button when the feature is disabled', () => {
    const { queryByTitle } = customRender(<RatingBar {...defaultProps} />, { initialState });
    expect(queryByTitle('Rating XRay')).toBeNull();
  });

  it('hides the rating x-ray button when there are no ratings', () => {
    const { queryByTitle } = customRender(<RatingBar {...defaultProps} />, { initialState });
    expect(queryByTitle('Rating XRay')).toBeNull();
  });

  it('displays the rating x-ray', () => {
    defaultProps.xrayEnabled = true;
    defaultProps.ratings.push({ rating: 'funny', count: 2, users: ['Bob'] });
    const { queryByTitle } = customRender(<RatingBar {...defaultProps} />, { initialState });
    expect(queryByTitle('Rating XRay')).not.toBeNull();
  });

  it('shows the rating menu', () => {
    const { queryByTestId } = customRender(<RatingBar {...defaultProps} />, { initialState });
    expect(queryByTestId('rating-bar')).not.toBeNull();
  });

  it('hides the rating menu if rating is disabled', () => {
    defaultProps.ratingDisabled = true;
    const { queryByTestId } = customRender(<RatingBar {...defaultProps} />, { initialState });
    expect(queryByTestId('rating-bar')).toBeNull();
  });

  it('hides the rating x-ray menu if the feature is disabled', () => {
    const { queryByTitle } = customRender(<RatingBar {...defaultProps} />, { initialState });
    expect(queryByTitle('Rating x-ray list')).toBeNull();
  });

  it('hides the rating x-ray menu if there are no ratings', () => {
    defaultProps.xrayEnabled = true;
    const { queryByTitle } = customRender(<RatingBar {...defaultProps} />, { initialState });
    expect(queryByTitle('Rating x-ray list')).toBeNull();
  });

  it('displays the rating x-ray menu', async () => {
    defaultProps.xrayEnabled = true;
    defaultProps.ratings.push({ rating: 'funny', count: 2, users: ['Bob'] });
    const { queryByTitle } = customRender(<RatingBar {...defaultProps} />, { initialState });

    await act(async () => {
      queryByTitle('Rating XRay').click();
    });
    expect(queryByTitle('Rating x-ray list')).not.toBeNull();
  });
});
