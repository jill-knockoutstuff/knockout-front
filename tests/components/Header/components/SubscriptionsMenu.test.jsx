/* eslint-disable no-underscore-dangle */
import React from 'react';
import axios from 'axios';
import { customRender, fireEvent } from '../../../custom_renderer';
import '@testing-library/jest-dom/extend-expect';
import SubscriptionsMenu from '../../../../src/components/Header/components/SubscriptionsMenu';

jest.mock('axios');

describe('SubscriptionsMenu component', () => {
  const initialState = {
    subscriptions: {
      threads: {},
      count: 0,
    },
  };

  it('displays the menu when the icon is clicked', () => {
    const { getByTitle, queryByText } = customRender(<SubscriptionsMenu />, { initialState });
    fireEvent.click(getByTitle('Subscriptions'));
    expect(queryByText('Subscriptions')).not.toBeNull();
  });

  it('displays an empty state', () => {
    const { getByTitle, queryByText } = customRender(<SubscriptionsMenu />, { initialState });
    fireEvent.click(getByTitle('Subscriptions'));
    expect(queryByText('No new posts')).not.toBeNull();
  });

  it('displays threads with unread posts', async () => {
    localStorage.__STORE__.currentUser = '1';
    axios.get.mockResolvedValue({
      data: {
        alerts: [
          {
            threadId: 0,
            threadIcon: 67,
            threadTitle: 'Thread',
            threadPostCount: 74,
            unreadPosts: 3,
            firstUnreadId: -1,
          },
          {
            threadId: 2,
            threadIcon: 27,
            threadTitle: 'Second Thread',
            threadPostCount: 26,
            unreadPosts: 7,
            firstUnreadId: -1,
          },
        ],
      },
    });
    const { getByTitle, queryByTitle, findByText } = customRender(<SubscriptionsMenu />);
    fireEvent.click(getByTitle('Subscriptions'));
    expect(await findByText('Thread')).not.toBeNull();
    expect(await findByText('3')).not.toBeNull();
    expect(await findByText('Second Thread')).not.toBeNull();
    expect(await findByText('7')).not.toBeNull();
    expect(queryByTitle('Subscriptions').querySelector('.link-notification')).toHaveTextContent(
      '2'
    );
  });

  it('shows if a thread is locked', async () => {
    localStorage.__STORE__.currentUser = '1';
    axios.get.mockResolvedValue({
      data: {
        alerts: [
          {
            threadId: 2,
            threadIcon: 27,
            threadTitle: 'Thread',
            threadPostCount: 26,
            threadLocked: true,
            unreadPosts: 7,
            firstUnreadId: -1,
          },
        ],
      },
    });
    const { getByTitle, findByTitle } = customRender(<SubscriptionsMenu />);
    fireEvent.click(getByTitle('Subscriptions'));
    expect(await findByTitle('Locked')).not.toBeNull();
  });
});
