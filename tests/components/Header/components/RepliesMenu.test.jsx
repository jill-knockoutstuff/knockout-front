import React from 'react';
import { customRender, fireEvent } from '../../../custom_renderer';
import '@testing-library/jest-dom/extend-expect';
import RepliesMenu from '../../../../src/components/Header/components/RepliesMenu';

describe('RepliesMenu component', () => {
  const initialStateWithMention = {
    notifications: {
      notifications: [
        {
          id: 8,
          type: 'POST_REPLY',
          userId: 1,
          read: true,
          createdAt: '2021-08-02T02:28:41.000Z',
          data: {
            id: 9,
            thread: {
              id: 1,
              title: 'Thread',
              iconId: 15,
              subforumId: 1,
            },
            page: 1,
            user: {
              id: 2,
              username: 'Bob',
              usergroup: 1,
              avatarUrl: 'none.webp',
            },
          },
        },
        {
          id: 2,
          type: 'MESSAGE',
          userId: 1,
          read: false,
          createdAt: '2021-08-02T00:17:46.000Z',
          data: {
            id: 1,
            messages: [
              {
                id: 18,
                conversationId: 1,
                user: {
                  id: 3,
                  username: 'Joe',
                  usergroup: 1,
                  avatarUrl: 'none.webp',
                },
              },
            ],
          },
        },
      ],
    },
  };

  it('displays the menu when the icon is clicked', () => {
    const { getByTitle, queryByText } = customRender(<RepliesMenu />, {
      initialState: initialStateWithMention,
    });
    fireEvent.click(getByTitle('Messages'));
    expect(queryByText('Messages')).not.toBeNull();
  });

  it('displays unread replies', () => {
    const { getByTitle, queryByText, queryByTitle } = customRender(<RepliesMenu />, {
      initialState: initialStateWithMention,
    });
    fireEvent.click(getByTitle('Messages'));
    expect(queryByText('Thread.')).not.toBeNull();
    expect(queryByText('Bob')).not.toBeNull();
    expect(queryByText('Joe')).not.toBeNull();
    expect(queryByTitle('Messages').querySelector('.link-notification')).toHaveTextContent('1');
  });
});
