/* eslint-disable no-underscore-dangle */
import React from 'react';
import axios from 'axios';
import { waitFor } from '@testing-library/react';
import { customRender, fireEvent } from '../../../custom_renderer';
import '@testing-library/jest-dom/extend-expect';
import MessageOfTheDay from '../../../../src/components/Header/components/MessageOfTheDay';

jest.mock('axios');

describe('MessageOfTheDay component', () => {
  let motd = {};
  beforeEach(() => {
    localStorage.clear();
    motd = {
      id: 1,
      message: 'A test message',
      buttonName: 'Read more',
      buttonLink: 'google.com',
    };
  });

  it('displays the motd', async () => {
    axios.get.mockResolvedValue({ data: [motd] });
    const { queryByText, findByText } = customRender(<MessageOfTheDay />);
    await findByText('A test message');
    expect(queryByText('A test message')).not.toBeNull();
    expect(queryByText('Read more')).not.toBeNull();
  });

  it('hides the button if a name is not returned', async () => {
    motd.buttonName = '';
    axios.get.mockResolvedValue({ data: [motd] });
    const { findByText, queryByText, queryByTestId } = customRender(<MessageOfTheDay />);
    await findByText('A test message');
    expect(queryByText('A test message')).not.toBeNull();
    expect(queryByTestId('motd-link')).toBeNull();
  });

  it('hides the button if a link is not returned', async () => {
    motd.buttonLink = '';
    axios.get.mockResolvedValue({ data: [motd] });
    const { queryByText, findByText } = customRender(<MessageOfTheDay />);
    await findByText('A test message');
    expect(queryByText('A test message')).not.toBeNull();
    expect(queryByText('Read more')).toBeNull();
  });

  it('dismisses the motd', async () => {
    axios.get.mockResolvedValue({ data: [motd] });
    const { queryByText, queryByTitle, getByTitle, findByText } = customRender(<MessageOfTheDay />);
    await findByText('A test message');
    fireEvent.click(getByTitle('Dismiss'));
    expect(queryByText('A test message')).toBeNull();
    expect(queryByTitle('Dismiss')).toBeNull();
  });

  it('hides the motd if previously dismissed', async () => {
    axios.get.mockResolvedValue({ data: [motd] });
    localStorage.__STORE__['motd-dismissed'] = '1';
    const { queryByText } = customRender(<MessageOfTheDay />);
    await waitFor(() => expect(axios.get).toHaveBeenCalledTimes(1));
    expect(queryByText('A test message')).toBeNull();
  });
});
